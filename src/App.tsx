import './App.css';
import { Route, Switch } from 'react-router-dom';
import Home from './pages/Home/Home';
import Navigation from './components/Navigation/Navigation';
import Resource from './pages/Resource/Resource';
import HomeButton from './components/HomeButton/HomeButton';

const App: React.FC = (): JSX.Element => {
  return (
    <>
      <HomeButton />
      <Navigation />
      <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/resource' component={Resource} />
      </Switch>
    </>
  );
};

export default App;
