import React, { useEffect } from 'react';
import firebase from '../helpers/firebase';

type CategoryType = { title: string; color: string; links: any[] };
type FetchedValue = { color: string; links: any[] };
interface ResourcesContextType {
  data: CategoryType[];
  getCategories: () => string[];
  getCategoryData: (category: string) => CategoryType | undefined;
}

const ResourcesContextDefaultValue: ResourcesContextType = {
  data: [],
  getCategories: () => [],
  getCategoryData: (category: string) => undefined,
};

const ResourcesContext = React.createContext(ResourcesContextDefaultValue);

const ResourcesProvider: React.FC = (props): JSX.Element => {
  const [data, setData] = React.useState<CategoryType[]>([]);

  useEffect(() => {
    firebase
      .database()
      .ref('/resources')
      .get()
      .then((snapshot) => {
        const fetchedData: CategoryType[] = [];
        Object.entries(snapshot.val()).forEach(([key, value]) => {
          const { color, links } = value as FetchedValue;
          const fetchedCategory = {
            title: key,
            color: color,
            links: links,
          };
          fetchedData.push(fetchedCategory);
        });
        setData(fetchedData);
      });
  }, []);

  const getCategories = (): string[] => {
    const categories: string[] = [];
    data.forEach((category) => {
      categories.push(category.title);
    });
    return categories;
  };

  const getCategoryData = (category: string): CategoryType | undefined => {
    const categoryData: CategoryType | undefined = data.find(
      (categoryData) => categoryData.title === category
    );
    return categoryData;
  };

  return (
    <ResourcesContext.Provider
      value={{ data, getCategories, getCategoryData }}
      {...props}
    />
  );
};

const useResources = () => React.useContext(ResourcesContext);

export { ResourcesProvider, useResources };
