import Book from '../../components/Book/Book';
import classes from './Library.module.css';

type categoryType = {
  title: string;
  backgroundColor: string;
  color: string;
  to: string;
};

interface LibraryProps {
  categories: categoryType[];
  className?: string;
}

const Library: React.FC<LibraryProps> = ({ categories }): JSX.Element => {
  console.log(categories);
  const books = categories.map((category) => {
    console.log(category);
    return (
      <Book
        title={category.title}
        color={category.color}
        backgroundColor={category.backgroundColor}
        to={category.to}
        key={category.title}
      />
    );
  });
  console.log(books);
  return <section className={classes.library}>{books}</section>;
};

export default Library;
export type { categoryType };
