import classes from './HomeButton.module.css';

import mainLogo from '../../assets/logo/TextLogo.png';
import smallLogo from '../../assets/logo/LogoSmall.png';
import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';

const HomeButton = () => {
  const [windowWidth, setWindowWidth] = useState(1440);
  useEffect(() => {
    setWindowWidth(window.innerWidth);
  }, []);
  if (windowWidth < 768) {
    return (
      <div className={classes['HomeButton--small']}>
        <Link to='/'>
          <img src={smallLogo} alt='Logo' />
        </Link>
      </div>
    );
  }
  return (
    <div className={classes.HomeButton}>
      <Link to='/'>
        <img src={mainLogo} alt='Logo' />
      </Link>
    </div>
  );
};

export default HomeButton;
