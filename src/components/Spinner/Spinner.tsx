import classes from './Spinner.module.css';

const Spinner = () => (
  <div className={classes.container}>
    <div className={classes.loader}>Loading...</div>
  </div>
);

export default Spinner;
