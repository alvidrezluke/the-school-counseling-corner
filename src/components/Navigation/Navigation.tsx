import { useEffect, useState } from 'react';
import { useResources } from '../../store/resources-context';
import classes from './Navigation.module.css';

const Navigation: React.FC = (): JSX.Element => {
  const [navItems, setNavItems] = useState<JSX.Element[]>();
  const resources = useResources();
  useEffect(() => {
    const generatedItems: JSX.Element[] = [];
    resources.getCategories().forEach((category) => {
      generatedItems.push(
        <li className={classes['navigation__item']}>
          <a
            href={`/resource/${encodeURI(category)}`}
            className={classes['navigation__link']}
          >
            {category}
          </a>
        </li>
      );
    });
    setNavItems(generatedItems);
  }, [resources]);
  return (
    <nav className={classes.nav}>
      <input
        type='checkbox'
        className={classes['navigation__checkbox']}
        id='navi-toggle'
      />

      <label htmlFor='navi-toggle' className={classes['navigation__button']}>
        <span className={classes['navigation__icon']}>&nbsp;</span>
      </label>

      <div className={classes['navigation__background']}>&nbsp;</div>

      <div className={classes['navigation__nav']}>
        <ul className={classes['navigation__list']}>
          <li className={classes['navigation__item']}>
            <a href='/' className={classes['navigation__link']}>
              Home
            </a>
          </li>
          {navItems}
        </ul>
      </div>
    </nav>
  );
};

export default Navigation;
