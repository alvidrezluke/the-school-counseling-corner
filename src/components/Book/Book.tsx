import { useHistory } from 'react-router-dom';
import classes from './Book.module.css';

interface BookProps {
  color: string;
  backgroundColor: string;
  title: string;
  to: string;
}

const Book: React.FC<BookProps> = ({
  backgroundColor,
  color,
  title,
  to,
}): JSX.Element => {
  const history = useHistory();
  const height = 100 - Math.round(Math.random() * 20) + '%';
  const handleNavigate = () => {
    history.push(to);
  };
  return (
    <div
      className={classes.book}
      style={{ backgroundColor: backgroundColor, height: height }}
      onClick={handleNavigate}
    >
      <h3
        className={classes['book__title']}
        style={{ color: color, backgroundColor: backgroundColor }}
      >
        {title}
      </h3>
    </div>
  );
};

export default Book;
