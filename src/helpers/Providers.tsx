import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { ResourcesProvider } from '../store/resources-context';

const Providers: React.FC = ({ children }): JSX.Element => {
  return (
    <React.StrictMode>
      <BrowserRouter>
        <ResourcesProvider>{children}</ResourcesProvider>
      </BrowserRouter>
    </React.StrictMode>
  );
};

export default Providers;
