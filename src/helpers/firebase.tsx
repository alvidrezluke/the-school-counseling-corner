import firebase from 'firebase/app';
import 'firebase/database';

const firebaseConfig = {
  apiKey: 'AIzaSyCqMALjJnVwCP6mpyM4cmL5ijBOtMVLYQk',
  authDomain: 'alvidrezschoolcounseling.firebaseapp.com',
  databaseURL: 'https://alvidrezschoolcounseling.firebaseio.com',
  projectId: 'alvidrezschoolcounseling',
  storageBucket: 'alvidrezschoolcounseling.appspot.com',
  messagingSenderId: '928103383717',
  appId: '1:928103383717:web:29267216aba8aa7844834c',
  measurementId: 'G-X9YBXGNH3H',
};

firebase.initializeApp(firebaseConfig);

export default firebase;
