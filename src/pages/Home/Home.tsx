import classes from './Home.module.css';

import Library from '../../containers/Library/Library';
import { useResources } from '../../store/resources-context';
import { useEffect, useState } from 'react';
import { categoryType } from '../../containers/Library/Library';

const Home: React.FC = (): JSX.Element => {
  const resources = useResources();
  const [books, setBooks] = useState<categoryType[]>([]);
  useEffect(() => {
    const fetchedBooks: categoryType[] = [];
    resources.data.forEach((resource) => {
      fetchedBooks.push({
        title: resource.title,
        backgroundColor: resource.color,
        color: '#fff',
        to: `/resource/${encodeURI(resource.title)}`,
      });
    });
    setBooks(fetchedBooks);
  }, [resources]);
  console.log(books);
  return (
    <main className={classes.home}>
      <h1 className={classes['home__header']}>
        The School
        <br />
        Counseling Corner
      </h1>
      <h2 className={classes['home__info']}>
        A library of resources for students and parents.
      </h2>
      <Library categories={books} />
    </main>
  );
};

export default Home;
