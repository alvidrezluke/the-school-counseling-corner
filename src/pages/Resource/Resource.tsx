import classes from './Resource.module.css';
import { useResources } from '../../store/resources-context';
import { useEffect, useState } from 'react';
import Spinner from '../../components/Spinner/Spinner';

interface ResourceProps {
  location: { pathname: string };
}

type LinkDataType = {
  color: string;
  title: string;
  links: Object;
};

const blankLinkData: LinkDataType = { color: '', title: '', links: {} };

const Resource: React.FC<ResourceProps> = ({ location }): JSX.Element => {
  const category = location.pathname.replace('/resource/', '');
  const resource = useResources();
  const [data, setData] = useState<LinkDataType>(blankLinkData);
  const [loading, setLoading] = useState<boolean>(true);
  useEffect(() => {
    const data = resource.getCategoryData(category);
    if (data !== undefined) {
      setLoading(false);
      setData(data);
    } else {
      setLoading(false);
      setData(blankLinkData);
    }
  }, [resource, category]);

  const linksToRender = [[], []] as JSX.Element[][];
  let firstColumn = true;
  Object.entries(data.links).forEach((linkData) => {
    const listedLinks = [] as JSX.Element[];
    const title = linkData[0];
    const subcategory = linkData[1];

    Object.entries(subcategory).forEach((link) => {
      listedLinks.push(
        <li
          key={link[0]}
          className={classes['category__subcategory--listitem']}
        >
          <a
            href={`${link[1]}`}
            className={classes['category__subcategory--link']}
          >
            {link[0]}
          </a>
        </li>
      );
    });

    linksToRender[firstColumn ? 0 : 1].push(
      <section
        key={title}
        className={classes['category__subcategory--container']}
      >
        <h2 className={classes['category__subcategory--header']}>{title}</h2>
        <ul className={classes['category__subcategory--list']}>
          {listedLinks}
        </ul>
      </section>
    );

    firstColumn = !firstColumn;
  });

  let singleColumn = false;

  if (linksToRender[1].length === 0) {
    singleColumn = true;
  }
  return (
    <main
      className={`${classes.category} ${
        singleColumn ? classes['category--single'] : ''
      }`}
    >
      {loading ? <Spinner /> : null}
      <h1 className={classes.category__header}>{category}</h1>
      <div className={classes['category__column']}>{linksToRender[0]}</div>
      <div className={classes['category__column']}>{linksToRender[1]}</div>
    </main>
  );
};

export default Resource;
